package hillel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amd
 */
public class GenericMethodTest {
    
    public GenericMethodTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of PopulateCollection method, of class GenericMethod.
     */
    @Test
    public void testPopulateCollection_String() {
        String[] arr = {"one", "two", "three"};
        Collection exp = Arrays.asList(arr);
        
        Collection val = new ArrayList();
        GenericMethod.PopulateCollection(arr, val);
        assertEquals(exp, val);

    }

    @Test
    public void testPopulateCollection_Integer() {
        Integer[] arr = {1, 2, 3};
        Collection exp = Arrays.asList(arr);
        
        Collection val = new ArrayList();
        GenericMethod.PopulateCollection(arr, val);
        assertEquals(exp, val);
    }

    
}
