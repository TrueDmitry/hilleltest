
package hillel;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author amd
 */
public class DistinctWordsTest {

    public DistinctWordsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    private void asertDistinctWords(String[] expArr, String testStr) throws IOException {
        Arrays.sort(expArr);
        Reader r = new StringReader(testStr);
        Object[] res = new DistinctWords().getWords(r).toArray();
        Arrays.sort(res);

        Assert.assertArrayEquals(expArr, res);
    }

    @Test
    public void testGetWords_same() throws IOException {
        String testStr = "word word word word";
        String[] resArr = {"word"};

        asertDistinctWords(resArr, testStr);
    }

    @Test
    public void testGetWords_diff() throws IOException {
        String testStr = "one two three four";
        String[] resArr = {"one", "two", "three", "four"};

        asertDistinctWords(resArr, testStr);
    }

    @Test
    public void testGetWords_newLine() throws IOException {
        String testStr = "one Two \n three four";
        String[] resArr = {"one", "Two", "three", "four"};

        asertDistinctWords(resArr, testStr);
    }

    @Test
    public void testGetWords_case() throws IOException {
        { // leading word with capital
            String testStr = "one Two two three four";
            String[] resArr = {"one", "Two", "three", "four"};
            asertDistinctWords(resArr, testStr);
        }
        { // leadin word with lowercase
            String testStr = "one two Two three four";
            String[] resArr = {"one", "two", "three", "four"};
            asertDistinctWords(resArr, testStr);
        }
    }

    @Test
    public void testGetWords_wastechars() throws IOException {
        //.,/-;:
        String[] resArr = {"one", "two", "three", "four"};
        String testStr = "one., /- two;three:four";

        asertDistinctWords(resArr, testStr);
    }

}
