package hillel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Collection;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 1. Write a program to find all distinct words from a text file. Ignore chars
 * like ".,/-;:" and Ignore case sensitivity.
 *
 * @author amd
 */
public class DistinctWords {

    Pattern pattern = Pattern.compile("\\w+");

    public Collection getWords(Reader r) throws IOException {
        Collection res = new TreeSet(String.CASE_INSENSITIVE_ORDER);

        BufferedReader br = new BufferedReader(r);
        
        String str;
        while ((str = br.readLine())!= null) {
            Matcher m = pattern.matcher(str);
            while (m.find()) {
                res.add(m.group());
            }
        }
        return res;
    }
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        String fileName = "./somefile.txt";
        try (Reader fr = new FileReader(new File(fileName))) {
            Collection words = new DistinctWords().getWords(fr);
            System.out.println(words);
        }
    }

}
