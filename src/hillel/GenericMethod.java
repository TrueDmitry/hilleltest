package hillel;


import java.util.Arrays;
import java.util.Collection;

/**
 * Write a generic method that takes an array of objects and a collection, and puts all objects in the array into the collection.
 * @author amd
 */
public class GenericMethod {

    
    public static <T> void PopulateCollection(T[] arr, Collection<T> collection) {
        collection.addAll(Arrays.asList(arr));
    }

}
