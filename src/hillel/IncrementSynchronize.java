package hillel;


import java.util.concurrent.Semaphore;

/**
 *
 * @author amd
 */
public class IncrementSynchronize {

    private final Object lock = new Object();
    private final Semaphore s = new Semaphore(1);

    private int value = 0;

    public synchronized int synchronizedOne() {
        return getNextValue();
    }

    public int synchronizedTwo() {
        synchronized (lock) {
            return getNextValue();
        }
    }

    public int synchronizedThree() {
        int res = 0;
        try {
            s.acquire();
            res = getNextValue();
            s.release();
        } catch (InterruptedException ex) {
        } // will not interrupt
        return res;
    }

    public int getNextValue() {
        return value++;
    }

}
