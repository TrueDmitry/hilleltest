package test;

interface testI {
    int IVAR = 10;
}

class A {
    public static int a = 5;
}

/**
 * @author amd
 */
public class Test extends A implements testI {
    int IVAR = 1;
    int a = 0;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Test test = new Test();
        A a = new Test();
        
        testI testi = new Test();
         
        System.out.println(a.a);
        System.out.println(test.a);
        
        System.out.println(test.IVAR);
        System.out.println(testi.IVAR);
        
    }
    
}
